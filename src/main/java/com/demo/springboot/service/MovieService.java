package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieListDto;

public interface MovieService {
    MovieListDto findMovie(String path, String link);
}
