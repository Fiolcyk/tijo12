package com.demo.springboot.service.serviceImpl;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.service.MovieService;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {
    @Override
    public MovieListDto findMovie(String path, String link) {

        List<MovieDto> movies = new ArrayList<>();
        List<String> temp = new ArrayList<>();
        List<String> word;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                temp.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String value : temp) {
            word = Arrays.asList(value.split(";"));
            if(word.get(1).toLowerCase().trim().contains(link.toLowerCase()) && word.get(4).trim().equals("false")){
                movies.add(new MovieDto(Integer.parseInt(word.get(0).trim()), word.get(1).trim(), Integer.parseInt(word.get(2).trim()), word.get(3).trim()));
            }
        }
        return new MovieListDto(movies);
    }
}